package com.dawnportal.authenticationservice.model;

import org.springframework.data.annotation.Id;

public class ResetPassword {
	@Id
	public String id;
	public String account;
	public long expires;
}
