package com.dawnportal.authenticationservice.model;

import com.dawnportal.core.model.Account;
import com.dawnportal.core.model.Company;

public class ContactAccount {
	public Account account;
	public Company company;
}
