package com.dawnportal.authenticationservice.controller;

import java.io.ByteArrayOutputStream;
import java.net.URLEncoder;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ServerWebExchange;

import com.dawnportal.authenticationservice.dao.ResetPasswordRepository;
import com.dawnportal.authenticationservice.model.AdminSearchResponse;
import com.dawnportal.authenticationservice.model.ContactAccount;
import com.dawnportal.authenticationservice.model.ResetPassword;
import com.dawnportal.core.dao.AccountRepository;
import com.dawnportal.core.dao.AuditRepository;
import com.dawnportal.core.dao.CompanyRepository;
import com.dawnportal.core.dao.DataCenterRepository;
import com.dawnportal.core.dao.PhysicalAccessRepository;
import com.dawnportal.core.model.Account;
import com.dawnportal.core.model.AuditLog;
import com.dawnportal.core.model.DataCenter;
import com.dawnportal.core.model.Invitation;
import com.dawnportal.core.model.Message;
import com.dawnportal.core.model.PhysicalAccess;
import com.dawnportal.core.model.PhysicalAccessEntry;
import com.dawnportal.core.model.PhysicalAccessRequest;
import com.dawnportal.core.model.Role;
import com.dawnportal.core.model.TokenDetails;
import com.dawnportal.core.model.UserProfile;
import com.dawnportal.core.service.AuditService;
import com.dawnportal.core.service.EmailService;
import com.dawnportal.core.service.LocaleService;
import com.dawnportal.core.service.RepositoryService;
import com.dawnportal.core.service.UserProfileService;
import com.dawnportal.core.service.ValidationService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.ImmutableMap;

import net.aholbrook.paseto.service.TokenService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

@RestController
@EnableReactiveMethodSecurity
public class AuthenticationController {
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	@Autowired
	private TokenService<TokenDetails> tokenService;
	
	@Autowired
	private TokenService<Invitation> invitationTokenService;
	
	@Autowired
	private AccountRepository accountRepository;
	
	@Autowired
	private CompanyRepository companyRepository;
	
	@Autowired
	private LocaleService localeService;
	
	@Autowired
	private RepositoryService repositoryService;
	
	@Autowired
	private ValidationService validationService;
	
	@Autowired
	private ResetPasswordRepository resetPasswordRepository;
	
	@Autowired
	private PhysicalAccessRepository physicalAccessRepository;
	
	@Autowired
	private DataCenterRepository dataCenterRepository;
	
	@Autowired
	private AuditRepository auditRepository;
	
	@Autowired
	private AuditService auditService;
	
	@Autowired
	private EmailService emailService;
	
	@Autowired
	private UserProfileService userProfileService;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@Value("${auth.header.token}")
    private String authTokenHeader;
	
	@Value("${auth.header.hash}")
    private String authHashHeader;
	
	@Value("${salesforce.domain}")
    private String salesforceDomain;
	
	@Value("${salesforce.token}")
    private String salesforceToken;
	
	@Value("${servicenow.token}")
    private String serviceNowToken;
	
	@Value("${servicenow.domain}")
    private String serviceNowDomain;
	
	@Value("${server.domain}")
	private String serverDomain;
	
	@Value("${superadmin.token}")
	private String superadminToken;
	
	@GetMapping("/search-domain")
	@PreAuthorize("hasAuthority('ADMIN') or hasAuthority('SUPERADMIN') or hasAuthority('CALLCENTERADMIN') or hasAuthority('CALLCENTERUSER')")
	public Mono<AdminSearchResponse> searchDomain(@RequestParam("domain") String domain)
	{
		return accountRepository.findFirstByRolesAndEmailEndsWith("ADMIN", domain)
		.flatMap(account -> {
			return companyRepository.findById(account.company)
					.map(company -> {
						ContactAccount contactAccount = new ContactAccount();
						
						contactAccount.account = account;
						contactAccount.company = company;
						
						AdminSearchResponse response = new AdminSearchResponse();
						
						response.portalContactAccount = contactAccount;
						
						return response;
					});
		});
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/audit-logs.xlsx", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	@PreAuthorize("hasAuthority('ADMIN') or hasAuthority('GUARDADMIN')")
	public Mono<Resource> downloadAuditLog(ServerWebExchange exchange) 
	{
		Workbook workbook = new SXSSFWorkbook();
		Sheet sheet = workbook.createSheet();
		AtomicInteger count = new AtomicInteger(0);
		
		Row headerRow = sheet.createRow(count.getAndIncrement());
		
		headerRow.createCell(0).setCellValue("Timestamp");
		headerRow.createCell(1).setCellValue("Action");
		headerRow.createCell(2).setCellValue("Company");
		headerRow.createCell(3).setCellValue("Data Center");
		headerRow.createCell(4).setCellValue("Actioner First Name");
		headerRow.createCell(5).setCellValue("Actioner Last Name");
		headerRow.createCell(6).setCellValue("Actioner Email");
		headerRow.createCell(7).setCellValue("Actioner Role");
		headerRow.createCell(8).setCellValue("Additional Data");
		
		return ReactiveSecurityContextHolder.getContext()
				.map(SecurityContext::getAuthentication)
	            .map(Authentication::getPrincipal)
	            .cast(Account.class)
	            .flatMapMany(account -> {
	            	return exchange.getFormData()
	            			.flatMapMany(data -> {
	            				String company = data.getFirst("company");
	            				String dataCenter = data.getFirst("dataCenter");
	            				String action = data.getFirst("action");
	            				String firstName = data.getFirst("firstName");
	            				String lastName = data.getFirst("lastName");
	            				String email = data.getFirst("email");
	            				String siteId = data.getFirst("siteId");
	            				long start = 0;
	            				long end = Long.MAX_VALUE;
	            				
	            				try
	            				{
	            					start = Long.parseLong(data.getFirst("startDate"));
	            				}
	            				catch(Exception e)
	            				{

	            				}
	            				
	            				try
	            				{
	            					end = Long.parseLong(data.getFirst("endDate"));
	            				}
	            				catch(Exception e)
	            				{
	            					
	            				}
	            				
	            				AuditLog example = new AuditLog();
	            				
	            				example.company = company;
	            				example.dataCenter = dataCenter;
	            				example.type = action;
	            				example.companyName = null;
	            				example.dataCenterName = null;
	            				example.accountFirstName = firstName;
	            				example.accountLastName = lastName;
	            				example.accountEmail = email;
	            				example.siteId = siteId;
	            				
	            				if(account.company != null && !account.company.trim().equals(""))
	        	            	{
	        	            		return auditRepository.findByCompanyAndTimestampBetweenOrderByTimestampDesc(account.company, start, end)
	        	            				.filter(log -> auditLogFilter(log, example));
	        	            	}
	        	            	
	        	            	return auditRepository.findByDataCenterAndTimestampBetweenOrderByTimestampDesc(account.dataCenter, start, end)
	        	            			.filter(log -> auditLogFilter(log, example));
	            			});
	            })
	            .map(log -> {
	            	Row newRow = sheet.createRow(count.getAndIncrement());
	            	Date timestamp = new Date(log.timestamp);
	            	
	            	newRow.createCell(0).setCellValue(timestamp.toString());
	            	newRow.createCell(1).setCellValue(log.type);
	            	newRow.createCell(2).setCellValue(log.companyName);
	            	newRow.createCell(3).setCellValue(log.dataCenterName);
	            	newRow.createCell(4).setCellValue(log.accountFirstName);
	            	newRow.createCell(5).setCellValue(log.accountLastName);
	            	newRow.createCell(6).setCellValue(log.accountEmail);
	            	newRow.createCell(7).setCellValue(log.role);
	            	
	            	try {
						newRow.createCell(8).setCellValue(objectMapper.writeValueAsString(log.data));
					} catch (JsonProcessingException e) {
					}
	            	
	            	return true;
	            })
	            .then(Mono.defer(() -> {
	            	try
	            	{
		            	ByteArrayOutputStream os = new ByteArrayOutputStream();
		            	
		                workbook.write(os);
		                
		                byte[] bytes = os.toByteArray();
		            	ByteArrayResource resource = new ByteArrayResource(bytes);
		            	
		            	workbook.close();
		            	
		            	return Mono.just(resource);
	            	}
	            	catch(Exception e)
	            	{
	            		throw localeService.createError("access.denied");
	            	}
	       
	            }));
	}
	
	@GetMapping("/reset")
	public Mono<Message> reset(@RequestParam("token") String token)
	{
		if(token == null || !token.equals(superadminToken))
		{
			return Mono.just(Message.NOT_AUTHORIZED);
		}
		
		return accountRepository.existsByEmail("ev-superadmin@zoozooweb.com")
		.flatMap(results -> {
			if(results)
			{
				return Mono.just(Message.OK_MESSAGE);
			}
			
			Account admin = new Account();
			
			admin.email = "ev-superadmin@zoozooweb.com";
			admin.password = passwordEncoder.encode("evoque");
			admin.id = UUID.randomUUID().toString();
			admin.roles.add(Role.SUPERADMIN);
			
			return accountRepository.save(admin)
					.map(account -> Message.OK_MESSAGE);
		});
	}
	
	@GetMapping("/logout")
	public Mono<Message> logout()
	{
		return ReactiveSecurityContextHolder.getContext()
				.map(SecurityContext::getAuthentication)
	            .map(Authentication::getPrincipal)
	            .cast(Account.class)
	            .flatMap(account -> accountRepository.findById(account.id))
	            .flatMap(account -> {
	            	return accountRepository.save(account);
	            })
	            .map(account -> {
	            	return Message.OK_MESSAGE;
	            });
	}
	
	@RequestMapping(value = "/ping", method = {RequestMethod.GET, RequestMethod.HEAD})
	public Mono<Message> ping()
	{
		return Mono.just(Message.OK_MESSAGE);
	}
	
	@PostMapping("/audit-logs")
	@PreAuthorize("hasAuthority('ADMIN') or hasAuthority('GUARDADMIN')")
	public Flux<AuditLog> auditLogs(ServerWebExchange exchange)
	{
		return ReactiveSecurityContextHolder.getContext()
	            .map(SecurityContext::getAuthentication)
	            .map(Authentication::getPrincipal)
	            .cast(Account.class)
	            .flatMapMany(account -> {
	            	return exchange.getFormData()
	            			.flatMapMany(data -> {
	            				String company = data.getFirst("company");
	            				String dataCenter = data.getFirst("dataCenter");
	            				String action = data.getFirst("action");
	            				String firstName = data.getFirst("firstName");
	            				String lastName = data.getFirst("lastName");
	            				String email = data.getFirst("email");
	            				String siteId = data.getFirst("siteId");
	            				long start = 0;
	            				long end = Long.MAX_VALUE;
	            				
	            				try
	            				{
	            					start = Long.parseLong(data.getFirst("startDate"));
	            				}
	            				catch(Exception e)
	            				{

	            				}
	            				
	            				try
	            				{
	            					end = Long.parseLong(data.getFirst("endDate"));
	            				}
	            				catch(Exception e)
	            				{
	            					
	            				}
	            				
	            				AuditLog example = new AuditLog();
	            				
	            				example.company = company;
	            				example.dataCenter = dataCenter;
	            				example.type = action;
	            				example.companyName = null;
	            				example.dataCenterName = null;
	            				example.accountFirstName = firstName;
	            				example.accountLastName = lastName;
	            				example.accountEmail = email;
	            				example.siteId = siteId;

	            				if(account.company != null && !account.company.trim().equals(""))
	        	            	{
	        	            		return auditRepository.findByCompanyAndTimestampBetweenOrderByTimestampDesc(account.company, start, end)
	        	            				.filter(log -> auditLogFilter(log, example));
	        	            	}
	        	            	
	        	            	return auditRepository.findByDataCenterAndTimestampBetweenOrderByTimestampDesc(account.dataCenter, start, end)
	        	            			.filter(log -> auditLogFilter(log, example));
	            			});
	            });
	}
	
	private boolean auditLogFilter(AuditLog log, AuditLog example)
	{
		if(example.company != null && !example.company.trim().equals("") && !example.company.equals(log.company))
		{
			return false;
		}
		
		if(example.dataCenter != null && !example.dataCenter.trim().equals("") && !example.dataCenter.equals(log.dataCenter))
		{
			return false;
		}
		
		if(example.siteId != null && !example.siteId.trim().equals("") && !example.siteId.equalsIgnoreCase(log.siteId))
		{
			return false;
		}
		
		if(example.type != null && !example.type.trim().equals("") && !example.type.equals(log.type))
		{
			return false;
		}
		
		if(example.accountFirstName != null && !example.accountFirstName.trim().equals("") && !example.accountFirstName.equalsIgnoreCase(log.accountFirstName))
		{
			return false;
		}
		
		if(example.accountLastName != null && !example.accountLastName.trim().equals("") && !example.accountLastName.equalsIgnoreCase(log.accountLastName))
		{
			return false;
		}
		
		if(example.accountEmail != null && !example.accountEmail.trim().equals("") && !example.accountEmail.equalsIgnoreCase(log.accountEmail))
		{
			return false;
		}
		
		return true;
	}
	
	@PostMapping("/login")
	public Mono<Message> login(ServerWebExchange exchange) {
	    return ReactiveSecurityContextHolder.getContext()
	            .map(SecurityContext::getAuthentication)
	            .map(Authentication::getPrincipal)
	            .cast(Account.class)
	            .flatMap(account -> {
	            	if(account.hash == null || account.hash.trim().equals(""))
	            	{
	            		account.hash = RandomStringUtils.randomAlphanumeric(32);
	            	}
            		
            		return accountRepository.save(account);
	            })
	            .flatMap(account -> {
					Map<String, Object> audit = new HashMap<String, Object>();
					
					audit.put("email", account.email);
					audit.put("firstName", account.firstName);
					audit.put("lastName", account.lastName);
					
					return auditService.audit(account.id, account.company, account.dataCenter, null, "UserLogin", account.calculateRole(), audit)
							.map(ignore -> account);
				})
	            .map(account -> {
	            	TokenDetails token = new TokenDetails(account);
					
					String encoded = tokenService.encode(token);
					
					exchange.getResponse().getHeaders().add(authHashHeader, account.hash);
					exchange.getResponse().getHeaders().add(authTokenHeader, encoded);
					
					return Message.OK_MESSAGE;
	            });
	}
	
	@PostMapping("/loginHash")
	public Mono<Message> loginHash(ServerWebExchange exchange)
	{
		return exchange.getFormData()
				.map(val -> val.getFirst("hash"))
				.switchIfEmpty(Mono.just(""))
				.flatMap(hash -> {
					return accountRepository.findByHash(hash)
							.switchIfEmpty(Mono.error(localeService.createError("login.hash.invalid")));
				})
				.map(account -> {
					TokenDetails token = new TokenDetails(account);
					
					String encoded = tokenService.encode(token);
					
					exchange.getResponse().getHeaders().add(authTokenHeader, encoded);
					
					return Message.OK_MESSAGE;
				});
	}
	
	@PostMapping("/changePassword")
	public Mono<Message> changePassword(ServerWebExchange exchange)
	{
		return exchange.getFormData()
		.flatMap(data -> {
			String password = data.getFirst("password");
			String id = data.getFirst("id");
			
			return resetPasswordRepository.findById(id)
				.switchIfEmpty(Mono.error(localeService.createError("access.denied")))
				.flatMap(resetPassword -> {
					if(resetPassword.expires < System.currentTimeMillis())
					{
						throw localeService.createError("access.denied");
					}
					
					return accountRepository.findById(resetPassword.account);
				})
				.switchIfEmpty(Mono.error(localeService.createError("access.denied")))
				.flatMap(account -> {
					account.password = passwordEncoder.encode(password);
					
					return accountRepository.save(account);
				});
		})
		.map(ignore -> Message.OK_MESSAGE);
	}
	
	@PostMapping("/forgotPassword")
	public Mono<Message> forgotPassword(ServerWebExchange exchange)
	{
		return exchange.getFormData()
		.flatMap(data -> {
			String email = data.getFirst("email");
			
			return accountRepository.findFirstByEmailIgnoreCase(email);
		})
		.switchIfEmpty(Mono.error(localeService.createError("forgotpassword.user.invalid")))
        .flatMap(account -> {
        	if(account.legacy)
        	{
        		throw localeService.createError("forgotpassword.user.invalid");
        	}
        	
			Map<String, Object> audit = new HashMap<String, Object>();
			
			audit.put("email", account.email);
			
			return auditService.audit(account.id, account.company, account.dataCenter, null, "UserForgotPassword", account.calculateRole(), audit)
					.map(ignore -> account);
		})
		.flatMap(account -> {
			if(account.email != null && !account.email.trim().equals(""))
			{
				ResetPassword reset = new ResetPassword();
				
				reset.id = (UUID.randomUUID().toString() + UUID.randomUUID().toString()).replace("-", "").toLowerCase();
				reset.account = account.id;
				reset.expires = System.currentTimeMillis() + 3l * 24l * 60l * 60l * 1000l;
				
				return resetPasswordRepository.save(reset)
				.flatMap(resetPassword -> {
					Map<String, Object> variables = new HashMap<String, Object>();
					
					variables.put("url", "https://" + serverDomain + "/#/auth/resetpw?id=" + reset.id);
					
					return emailService.sendEmail(account.email, "Reset Evoque Password", "resetPassword", variables);
				})
				.map(ignore -> Message.OK_MESSAGE);
			}
			
			return Mono.just(Message.OK_MESSAGE);
		});
	}
	
	@PostMapping("/send-notification")
	@PreAuthorize("hasAuthority('SUPERADMIN') or hasAuthority('CALLCENTERADMIN') or hasAuthority('CALLCENTERUSER')")
	public Mono<Message> sendNotification(@RequestBody JsonNode node)
	{
		return emailService.sendEmail(node)
		.map(ignore -> Message.OK_MESSAGE);
	}
	
	@PostMapping("/profile")
	@PreAuthorize("hasAuthority('ADMIN') or hasAuthority('SUPERADMIN') or hasAuthority('CALLCENTERADMIN') or hasAuthority('CALLCENTERUSER')")
	public Mono<UserProfile> profile(ServerWebExchange exchange)
	{
		return exchange.getFormData()
				.flatMap(data -> {
					String email = data.getFirst("email");
					
					return accountRepository.findFirstByEmailIgnoreCase(email);
				})
				.flatMap(account -> userProfileService.accountToProfile(account));
	}
	
	@GetMapping("/profile")
	public Mono<UserProfile> profile()
	{
		return ReactiveSecurityContextHolder.getContext()
		.map(SecurityContext::getAuthentication)
        .map(Authentication::getPrincipal)
        .cast(Account.class)
        .switchIfEmpty(Mono.error(localeService.createError("access.denied")))
        .flatMap(account -> accountRepository.findById(account.id))
        .flatMap(account -> userProfileService.accountToProfile(account));
	}
	
	@GetMapping("/fix-physical-access")
	public Mono<Message> fixPhysicalAccess()
	{
		physicalAccessRepository.findAll()
		.subscribeOn(Schedulers.elastic())
		.flatMap(physicalAccess -> {
			if(physicalAccess.startDatetime != null)
			{
				if(physicalAccess.startDatetime.length() > 16)
				{
					physicalAccess.startDatetime = physicalAccess.startDatetime.substring(0, 16);
				}
			}
			if(physicalAccess.endDatetime != null)
			{
				if(physicalAccess.endDatetime.length() > 16)
				{
					physicalAccess.endDatetime = physicalAccess.endDatetime.substring(0, 16);
				}
			}
			
			if(physicalAccess.user != null)
			{
				return setUserOnPhysicalAccess(physicalAccess, physicalAccess.user)
				.flatMap(access -> {
					return physicalAccessRepository.save(access);
				});
			}
			
			return physicalAccessRepository.save(physicalAccess);
		})
		.doOnComplete(() -> System.out.println("Complete!"))
		.subscribe();
		
		return Mono.just(Message.OK_MESSAGE);
	}
	
	@PostMapping("/access-list")
	@PreAuthorize("hasAuthority('GUARDADMIN') or hasAuthority('GUARDUSER')")
	public Flux<PhysicalAccessEntry> accessListSearch(ServerWebExchange exchange)
	{
		return ReactiveSecurityContextHolder.getContext()
		        .map(SecurityContext::getAuthentication)
		        .map(Authentication::getPrincipal)
		        .cast(Account.class)
		        .flatMapMany(account -> {
					return exchange.getFormData()
		    			.flatMapMany(data -> {
		    				String firstName = data.getFirst("firstName");
		    				String lastName = data.getFirst("lastName");
		    				String email = data.getFirst("email");
		    				String siteId = data.getFirst("siteId");
		    				PhysicalAccess tempExample = new PhysicalAccess();
		    				    
		    				tempExample.currentAccess = null;
		    				tempExample.firstName = firstName;
		    				tempExample.lastName = lastName;
		    				tempExample.email = email;
		    				tempExample.dataCenter = account.dataCenter;
		    				tempExample.siteId = siteId;
		    				
		    				ExampleMatcher caseInsensitiveExampleMatcher = ExampleMatcher.matchingAll().withIgnoreCase();
		    			    Example<PhysicalAccess> accessExample = Example.of(tempExample, caseInsensitiveExampleMatcher);
		    			    
		    				return physicalAccessRepository.findAll(accessExample)
			        				.filter(access -> !access.currentAccess.equals("None") &&
			        						(siteId == null || siteId.equals(access.siteId)))
			        				.filterWhen(access -> physicalAccessFilter(access, true))
			        				.map(access -> new PhysicalAccessEntry(access));
		    			});
		        });
	}
	
	
	@GetMapping("/access-list/{company}")
	@PreAuthorize("hasAuthority('GUARDADMIN') or hasAuthority('GUARDUSER')")
	public Flux<PhysicalAccessEntry> dataCenterAccessList(@PathVariable("company") String company)
	{
		return ReactiveSecurityContextHolder.getContext()
		        .map(SecurityContext::getAuthentication)
		        .map(Authentication::getPrincipal)
		        .cast(Account.class)
		        .flatMapMany(account -> accountRepository.findByCompany(company)
		        		.filter(user -> user.enabled)
		        		.flatMap(user -> physicalAccessRepository.findFirstByUserAndDataCenter(user.id, account.dataCenter)
		        				.filterWhen(access -> physicalAccessFilter(access, true))
		        				.filter(access -> access.currentAccess != null && !access.currentAccess.equalsIgnoreCase("None"))
		        	        	.map(access -> new PhysicalAccessEntry(user, access))
		        		)
		        		.concatWith(physicalAccessRepository.findByCurrentAccessAndCompanyAndDataCenter("Temp", company, account.dataCenter)
		        				.filterWhen(access -> physicalAccessFilter(access, true))
		    	        		.map(access -> new PhysicalAccessEntry(access)))
		        )
		        .sort(Comparator.comparing(PhysicalAccessEntry::getName, 
		        		  Comparator.nullsLast(Comparator.naturalOrder())));
	}
	
	@GetMapping("/access-list/{company}/{siteId}")
	@PreAuthorize("hasAuthority('GUARDADMIN') or hasAuthority('GUARDUSER')")
	public Flux<PhysicalAccessEntry> dataCenterAccessListByCompanyAndSiteId(@PathVariable("company") String company, @PathVariable("siteId") String siteId)
	{
		return ReactiveSecurityContextHolder.getContext()
		        .map(SecurityContext::getAuthentication)
		        .map(Authentication::getPrincipal)
		        .cast(Account.class)
		        .flatMapMany(account -> accountRepository.findByCompanyAndSiteIds(company, siteId)
		        		.filter(user -> user.enabled)
		        		.flatMap(user -> physicalAccessRepository.findFirstByUserAndDataCenter(user.id, account.dataCenter)
		        				.filterWhen(access -> physicalAccessFilter(access, true))
		        				.filter(access -> access.currentAccess != null && !access.currentAccess.equalsIgnoreCase("None"))
		        	        	.map(access -> new PhysicalAccessEntry(user, access))
		        		)
		        		.concatWith(physicalAccessRepository.findByCurrentAccessAndCompanyAndDataCenterAndSiteId("Temp", company, account.dataCenter, siteId)
		        				.filterWhen(access -> physicalAccessFilter(access, true))
		    	        		.map(access -> new PhysicalAccessEntry(access)))
		        )
		        .sort(Comparator.comparing(PhysicalAccessEntry::getName, 
		        		  Comparator.nullsLast(Comparator.naturalOrder())));
	}
	
	@GetMapping("/access-list/{company}/{dataCenter}/{siteId}")
	@PreAuthorize("hasAuthority('CALLCENTERADMIN') or hasAuthority('CALLCENTERUSER')")
	public Flux<PhysicalAccessEntry> dataCenterAccessListByCompanyDataCenterAndSiteId(@PathVariable("company") String company, @PathVariable("dataCenter") String dataCenter, @PathVariable("siteId") String siteId)
	{
		return ReactiveSecurityContextHolder.getContext()
		        .map(SecurityContext::getAuthentication)
		        .map(Authentication::getPrincipal)
		        .cast(Account.class)
		        .flatMapMany(account -> accountRepository.findByCompanyAndSiteIds(company, siteId)
		        		.filter(user -> user.enabled)
		        		.flatMap(user -> physicalAccessRepository.findFirstByUserAndDataCenter(user.id, dataCenter)
		        			.filterWhen(access -> physicalAccessFilter(access, false))
		        				.switchIfEmpty(Mono.just(new PhysicalAccess()))
		        	        	.map(access -> new PhysicalAccessEntry(user, access))
		        		)
		        		.concatWith(physicalAccessRepository.findByCurrentAccessAndCompanyAndDataCenterAndSiteId("Temp", company, dataCenter, siteId)
		        				.filterWhen(access -> physicalAccessFilter(access, false))
		    	        		.map(access -> new PhysicalAccessEntry(access)))
		        )
		        .sort(Comparator.comparing(PhysicalAccessEntry::getName,  
		        		  Comparator.nullsLast(Comparator.naturalOrder())));
	}
	
	@GetMapping("/reactivate-user/{userId}")
	@PreAuthorize("hasAuthority('SUPERADMIN') or hasAuthority('ADMIN') or hasAuthority('CALLCENTERADMIN')")
	public Mono<Message> reactivateUser(@PathVariable("userId") String userId)
	{
		return ReactiveSecurityContextHolder.getContext()
		        .map(SecurityContext::getAuthentication)
		        .map(Authentication::getPrincipal)
		        .cast(Account.class)
		        .flatMap(account -> {
		        	return accountRepository.findById(userId)
		        	.flatMap(user -> {
		        		if(account.calculateRole() == Role.SUPERADMIN || (account.calculateRole() == Role.ADMIN && account.company.equals(user.company))
		        				|| (account.calculateRole() == Role.CALLCENTERADMIN && (user.calculateRole() == Role.CALLCENTERADMIN || user.calculateRole() == Role.CALLCENTERUSER)))
		        		{
		        			user.enabled = true;
		        			user.portalStatus = user.password != null ? "Registered" : "";
		        			
		        			return accountRepository.save(user)
		        					.flatMap(savedAccount -> changeSalesforceProfileStatus(savedAccount, user.password != null ? "Registered" : ""))
		        					.then(Mono.just(account));
		        		}
		        		
		        		throw localeService.createError("access.denied");
		        	})
		        	.map(ignore2 -> account);
		        	
		        })
		        .flatMap(account -> auditService.audit(account.id, account.company, account.dataCenter, null, "UserReactivated", account.calculateRole(), ImmutableMap.of("userId", userId)))
				.map(ignore -> Message.OK_MESSAGE);
	}
	
	@GetMapping("/deactivate-user/{userId}")
	@PreAuthorize("hasAuthority('SUPERADMIN') or hasAuthority('ADMIN') or hasAuthority('CALLCENTERADMIN')")
	public Mono<Message> deactivateUser(@PathVariable("userId") String userId)
	{
		return ReactiveSecurityContextHolder.getContext()
		        .map(SecurityContext::getAuthentication)
		        .map(Authentication::getPrincipal)
		        .cast(Account.class)
		        .flatMap(account -> {
		        	return accountRepository.findById(userId)
		        	.flatMap(user -> {
		        		if(account.calculateRole() == Role.SUPERADMIN || (account.calculateRole() == Role.ADMIN && account.company.equals(user.company))
		        				|| (account.calculateRole() == Role.CALLCENTERADMIN && (user.calculateRole() == Role.CALLCENTERADMIN || user.calculateRole() == Role.CALLCENTERUSER)))
		        		{
		        			user.enabled = false;
		        			user.portalStatus = "Deactivated";
		        			
		        			return accountRepository.save(user)
		        					.flatMap(savedAccount -> changeSalesforceProfileStatus(savedAccount, "Deactivated"))
		        					.then(Mono.just(account));
		        		}
		        		
		        		throw localeService.createError("access.denied");
		        	})
		        	.map(ignore2 -> account);
		        	
		        })
		        .flatMap(account -> auditService.audit(account.id, account.company, account.dataCenter, null, "UserDeactivated", account.calculateRole(), ImmutableMap.of("userId", userId)))
				.map(ignore -> Message.OK_MESSAGE);
	}
	
	@GetMapping("/company-user-list/{dataCenter}")
	@PreAuthorize("hasAuthority('ADMIN')")
	public Flux<PhysicalAccessEntry> companyAccessList(@PathVariable("dataCenter") String dataCenter)
	{	
		return ReactiveSecurityContextHolder.getContext()
        .map(SecurityContext::getAuthentication)
        .map(Authentication::getPrincipal)
        .cast(Account.class)
        .flatMapMany(account -> 
        	accountRepository.findByCompany(account.company)
	        .filter(user -> user.enabled)
	        .flatMap(user -> {
	        	return physicalAccessRepository.findFirstByUserAndDataCenter(user.id, dataCenter)
	        			.filterWhen(access -> physicalAccessFilter(access, false))
	        	.switchIfEmpty(Mono.just(new PhysicalAccess()))
	        	.map(access -> new PhysicalAccessEntry(user, access));
	        })
	        .concatWith(physicalAccessRepository.findByCurrentAccessAndCompanyAndDataCenter("Temp", account.company, dataCenter)
	        		.filterWhen(access -> physicalAccessFilter(access, false))
	        		.map(access -> new PhysicalAccessEntry(access))
	        )
	    )
        .sort(Comparator.comparing(PhysicalAccessEntry::getName, 
      		  Comparator.nullsLast(Comparator.naturalOrder())));
	}
	
	@GetMapping("/company-user-list/{dataCenter}/{siteId}")
	@PreAuthorize("hasAuthority('ADMIN')")
	public Flux<PhysicalAccessEntry> companyAccessListBySiteId(@PathVariable("dataCenter") String dataCenter, @PathVariable("siteId") String siteId)
	{
		return ReactiveSecurityContextHolder.getContext()
        .map(SecurityContext::getAuthentication)
        .map(Authentication::getPrincipal)
        .cast(Account.class)
        .flatMapMany(account -> 
        	accountRepository.findByCompanyAndSiteIds(account.company, siteId)
        	.filter(user -> user.enabled)
	        .flatMap(user -> {
	        	return physicalAccessRepository.findFirstByUserAndDataCenter(user.id, dataCenter)
	        			.filterWhen(access -> physicalAccessFilter(access, false))
	        	.switchIfEmpty(Mono.just(new PhysicalAccess()))
	        	.map(access -> new PhysicalAccessEntry(user, access));
	        })
	        .concatWith(physicalAccessRepository.findByCurrentAccessAndCompanyAndDataCenterAndSiteId("Temp", account.company, dataCenter, siteId)
	        		.filterWhen(access -> physicalAccessFilter(access, false))
	        		.map(access -> new PhysicalAccessEntry(access)))
	    )
        .sort(Comparator.comparing(PhysicalAccessEntry::getName, 
      		  Comparator.nullsLast(Comparator.naturalOrder())));
	}
	
	@GetMapping("/company-user-list-by-site/{siteId}")
	@PreAuthorize("hasAuthority('ADMIN')")
	public Flux<PhysicalAccessEntry> companyAccessListBySiteId(@PathVariable("siteId") String siteId)
	{
		return dataCenterRepository.findFirstBySiteIds(siteId)
				.defaultIfEmpty(new DataCenter())
				.flatMapMany(dataCenter -> 
				ReactiveSecurityContextHolder.getContext()
			        .map(SecurityContext::getAuthentication)
			        .map(Authentication::getPrincipal)
			        .cast(Account.class)
			        .flatMapMany(account -> 
			        	accountRepository.findByCompanyAndSiteIds(account.company, siteId)
			        	.filter(user -> user.enabled)
				        .flatMap(user -> {
				        	return physicalAccessRepository.findFirstByUserAndDataCenter(user.id, dataCenter.salesforceId)
				        			.filterWhen(access -> physicalAccessFilter(access, false))
				        	.switchIfEmpty(Mono.just(new PhysicalAccess()))
				        	.map(access -> new PhysicalAccessEntry(user, access));
				        })
				        .concatWith(physicalAccessRepository.findByCurrentAccessAndCompanyAndSiteId("Temp", account.company, siteId)
				        		.filterWhen(access -> physicalAccessFilter(access, false))
				        		.map(access -> new PhysicalAccessEntry(access)))
			        )
			        .sort(Comparator.comparing(PhysicalAccessEntry::getName, 
			      		  Comparator.nullsLast(Comparator.naturalOrder()))));
	}
	
	private Mono<Boolean> physicalAccessFilter(PhysicalAccess access, boolean onlyToday)
	{
		if(access.currentAccess != null && (access.currentAccess.equalsIgnoreCase("Scheduled") || access.currentAccess.equalsIgnoreCase("Temp")))
		{
			return dataCenterRepository.findById(access.dataCenter)
				.defaultIfEmpty(new DataCenter())
				.map(dataCenter -> {
					ZoneId zoneId = ZoneId.of("GMT");
					String startTime = access.startDatetime + ":00Z";
					String endTime = access.endDatetime + ":00Z";
					ZonedDateTime now = ZonedDateTime.now(zoneId);
					
					try
					{
						zoneId = ZoneId.of(dataCenter.timezoneId);
						String offsetId = zoneId.getRules().getOffset(now.toInstant()).getId();
						startTime = access.startDatetime + ":00" + offsetId;
						endTime = access.endDatetime + ":00" + offsetId;
					}
					catch(Exception e)
					{
						startTime = access.startDatetime + ":00Z";
						endTime = access.endDatetime + ":00Z";
					}
					
					try
					{		
						ZonedDateTime endDateTime = ZonedDateTime.parse(endTime, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
						
						if(!endDateTime.isAfter(now))
						{
							return false;
						}
						
						if(onlyToday)
						{
							ZonedDateTime startDateTime = ZonedDateTime.parse(startTime, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
							
							if(!startDateTime.isBefore(now))
							{
								return false;
							}
						}
					}
					catch(Exception e)
					{
					}
					
					return true;
				});
		}
		
		return Mono.just(true);
	}
	
	@PostMapping("/checkout")
	@PreAuthorize("hasAuthority('GUARDADMIN') or hasAuthority('GUARDUSER')")
	public Mono<Message> checkout(ServerWebExchange exchange)
	{
		return ReactiveSecurityContextHolder.getContext()
		        .map(SecurityContext::getAuthentication)
		        .map(Authentication::getPrincipal)
		        .cast(Account.class)
		        .flatMap(account -> {
		        	return exchange.getFormData()
		        		.flatMap(dataForm -> {
		        			String userId = dataForm.getFirst("userId");
		        			String badgeNumber = dataForm.getFirst("badgeNo");
		        			
		        			if(badgeNumber == null || badgeNumber.trim().equals(""))
		        			{
		        				throw localeService.createError("checkin.badge.invalid");
		        			}
		        			
		        			if(userId == null || userId.trim().equals(""))
		        			{
		        				throw localeService.createError("checkin.user.invalid");
		        			}
		        			
		        			return physicalAccessRepository.findFirstByUserAndDataCenter(userId, account.dataCenter)
		        					.switchIfEmpty(physicalAccessRepository.findFirstByEmailIgnoreCaseAndDataCenter(userId, account.dataCenter))
		        					.switchIfEmpty(Mono.error(localeService.createError("checkin.user.invalid")))
		        					.flatMap(access -> {
		        						if(!badgeNumber.equals(access.badgeNo))
		        						{
		        							throw localeService.createError("checkin.badge.mismatch");
		        						}
		        						
		        						access.badgeNo = null;
		        						access.checkedIn = false;
		        						
		        						return physicalAccessRepository.save(access);
		        					})
		        					.flatMap(access -> {
		            					Map<String, Object> audit = new HashMap<String, Object>();
		            					
		            					audit.put("user", access.user);
		            					audit.put("company", access.company);
		            					audit.put("dataCenter", access.dataCenter);
		            					audit.put("siteId", access.siteId);
		            					audit.put("badgeNo", badgeNumber);
		            					
		            					return auditService.audit(account.id, account.company, account.dataCenter, access.siteId, "SiteAccessCheckout", account.calculateRole(), audit);
		            				});
		        		});
		        })
		        .map(ignore -> Message.OK_MESSAGE);
	}
	
	@PostMapping("/checkin")
	@PreAuthorize("hasAuthority('GUARDADMIN') or hasAuthority('GUARDUSER')")
	public Mono<Message> checkin(ServerWebExchange exchange)
	{
		return ReactiveSecurityContextHolder.getContext()
		        .map(SecurityContext::getAuthentication)
		        .map(Authentication::getPrincipal)
		        .cast(Account.class)
		        .flatMap(account -> {
		        	return exchange.getFormData()
		        		.flatMap(dataForm -> {
		        			String userId = dataForm.getFirst("userId");
		        			String badgeNumber = dataForm.getFirst("badgeNo");
		        			
		        			if(badgeNumber == null || badgeNumber.trim().equals(""))
		        			{
		        				throw localeService.createError("checkin.badge.invalid");
		        			}
		        			
		        			if(userId == null || userId.trim().equals(""))
		        			{
		        				throw localeService.createError("checkin.user.invalid");
		        			}
		        			
		        			return physicalAccessRepository.findFirstByUserAndDataCenter(userId, account.dataCenter)
		        					.switchIfEmpty(physicalAccessRepository.findFirstByEmailIgnoreCaseAndDataCenter(userId, account.dataCenter))
		        					.switchIfEmpty(Mono.error(localeService.createError("checkin.user.invalid")))
		        					.flatMap(access -> {
		        						access.badgeNo = badgeNumber;
		        						access.checkedIn = true;
		        						
		        						return physicalAccessRepository.save(access);
		        					})
		        					.flatMap(access -> {
		            					Map<String, Object> audit = new HashMap<String, Object>();
		            					
		            					audit.put("user", access.user);
		            					audit.put("company", access.company);
		            					audit.put("dataCenter", access.dataCenter);
		            					audit.put("siteId", access.siteId);
		            					audit.put("badgeNo", badgeNumber);
		            					audit.put("email", access.email);
		            					audit.put("firstName", access.firstName);
		            					audit.put("lastName", access.lastName);
		            					audit.put("companyName", access.companyName);
		            					
		            					return auditService.audit(account.id, account.company, account.dataCenter, access.siteId, "SiteAccessCheckin", account.calculateRole(), audit);
		            				});
		        		});
		        })
		        .map(ignore -> Message.OK_MESSAGE);
	}
	
	@PostMapping("/set-temp-access/{company}")
	@PreAuthorize("hasAuthority('CALLCENTERADMIN') or hasAuthority('CALLCENTERUSER')")
	public Mono<Message> setTempPhysicalAccess(@RequestBody PhysicalAccessRequest request, @PathVariable("company") String company)
	{
		return ReactiveSecurityContextHolder.getContext()
        .map(SecurityContext::getAuthentication)
        .map(Authentication::getPrincipal)
        .cast(Account.class)
        .flatMap(account -> {
        	return physicalAccessRepository.findFirstByEmailIgnoreCaseAndDataCenterAndCompany(request.siteAccess.email, request.location.dataCenter, company)
        	.switchIfEmpty(Mono.defer(() -> {
        		PhysicalAccess access = new PhysicalAccess();
        		
        		return repositoryService.generateUniqueId(physicalAccessRepository)
        				.map(id -> {
        					access.id = id;
        					
        					return access;
        				});
        	}))
        	.zipWith(dataCenterRepository.findById(request.location.dataCenter))
        	.flatMap(tuple -> {
        		PhysicalAccess access = tuple.getT1();
        		DataCenter dataCenter = tuple.getT2();
        		
				access.fromTempRequestInfo(request.siteAccess, request.location, company, dataCenter.international != null ? dataCenter.international : false);
				
				String startTime = access.startDatetime + ":00Z";
				String endTime = access.endDatetime + ":00Z";
				
				OffsetDateTime endDateTime = OffsetDateTime.parse(endTime);
				OffsetDateTime startDateTime = OffsetDateTime.parse(startTime);
				
				long days = startDateTime.until(endDateTime, ChronoUnit.DAYS);
				
				if(days > 30)
				{
					throw localeService.createError("physical.access.invalid");
				}
				
				return physicalAccessRepository.save(access);
        	})
        	.flatMap(access -> {
				Map<String, Object> audit = new HashMap<String, Object>();
				
				audit.put("currentAccess", access.currentAccess);
				audit.put("currentAccessDateRange", access.currentAccessDateRange);
				audit.put("startDatetime", access.startDatetime);
				audit.put("endDatetime", access.endDatetime);
				audit.put("company", access.company);
				audit.put("dataCenter", access.dataCenter);
				audit.put("siteId", access.siteId);
				audit.put("firstName", access.firstName);
				audit.put("lastName", access.lastName);
				audit.put("email", access.email);
				audit.put("companyName", access.companyName);
				
				return auditService.audit(account.id, access.company, access.dataCenter, access.siteId, "ChangeUserSiteAccess", account.calculateRole(), audit)
						.map(ignore -> account);
			});
        })
        .then(Mono.just(Message.OK_MESSAGE));
	}
	
	private Mono<PhysicalAccess> setUserOnPhysicalAccess(PhysicalAccess access, String userId)
	{
		return accountRepository.findById(userId)
		.map(account -> {
			access.firstName = account.firstName;
			access.lastName = account.lastName;
			access.email = account.email;
			access.user = userId;
			access.role = (account.roles != null && !account.roles.isEmpty() ? account.roles.iterator().next() : null);
			
			return access;
		});
	}
	
	@PostMapping("/set-access/{company}")
	@PreAuthorize("hasAuthority('CALLCENTERADMIN') or hasAuthority('CALLCENTERUSER')")
	public Mono<Message> setPhysicalAccess(@RequestBody PhysicalAccessRequest request, @PathVariable("company") String company)
	{
		if(request.companyUserList == null)
		{
			request.companyUserList = new ArrayList<>();
		}
		
		return ReactiveSecurityContextHolder.getContext()
        .map(SecurityContext::getAuthentication)
        .map(Authentication::getPrincipal)
        .cast(Account.class)
        .flatMapMany(account -> {
        	return Flux.fromIterable(request.companyUserList)
        	.flatMap(userId -> 
        	{
        		return physicalAccessRepository.findFirstByUserAndDataCenter(userId, request.location.dataCenter)
        				.switchIfEmpty(Mono.defer(() -> {
        	        		PhysicalAccess access = new PhysicalAccess();
        	        		
        	        		return repositoryService.generateUniqueId(physicalAccessRepository)
        	        				.map(id -> {
        	        					access.id = id;
        	        					
        	        					return access;
        	        				});
        	        	}))
        				.flatMap(access -> setUserOnPhysicalAccess(access, userId))
        				.flatMap(access -> {
        					access.fromRequestInfo(request.siteAccess, request.location, userId, company);
        					
        					if(access.currentAccess.equals("Scheduled"))
        					{
        						String startTime = access.startDatetime + ":00Z";
        						String endTime = access.endDatetime + ":00Z";
        						
        						OffsetDateTime endDateTime = OffsetDateTime.parse(endTime);
        						OffsetDateTime startDateTime = OffsetDateTime.parse(startTime);
        						
        						long days = startDateTime.until(endDateTime, ChronoUnit.DAYS);
        						
        						if(days > 30)
        						{
        							throw localeService.createError("physical.access.invalid");
        						}
        					}
        					
        					return physicalAccessRepository.save(access);
        				})
        		        .flatMap(access -> {
        					Map<String, Object> audit = new HashMap<String, Object>();
        					
        					audit.put("currentAccess", access.currentAccess);
        					audit.put("currentAccessDateRange", access.currentAccessDateRange);
        					audit.put("startDatetime", access.startDatetime);
        					audit.put("endDatetime", access.endDatetime);
        					audit.put("user", access.user);
        					audit.put("company", access.company);
        					audit.put("dataCenter", access.dataCenter);
        					audit.put("siteId", access.siteId);
        					
        					return auditService.audit(account.id, access.company, access.dataCenter, access.siteId, "ChangeUserSiteAccess", account.calculateRole(), audit)
        							.map(ignore -> account);
        				});
        	});
        
        })
        .then(Mono.just(Message.OK_MESSAGE));
	}
	
	@PostMapping("/set-temp-access")
	@PreAuthorize("hasAuthority('ADMIN')")
	public Mono<Message> setTempPhysicalAccess(@RequestBody PhysicalAccessRequest request)
	{
		return ReactiveSecurityContextHolder.getContext()
        .map(SecurityContext::getAuthentication)
        .map(Authentication::getPrincipal)
        .cast(Account.class)
        .flatMap(account -> {
        	return physicalAccessRepository.findFirstByEmailIgnoreCaseAndDataCenterAndCompany(request.siteAccess.email, request.location.dataCenter, account.company)
        	.switchIfEmpty(Mono.defer(() -> {
        		PhysicalAccess access = new PhysicalAccess();
        		
        		return repositoryService.generateUniqueId(physicalAccessRepository)
        				.map(id -> {
        					access.id = id;
        					
        					return access;
        				});
        	}))
        	.zipWith(dataCenterRepository.findById(request.location.dataCenter))
        	.flatMap(tuple -> {
        		PhysicalAccess access = tuple.getT1();
        		DataCenter dataCenter = tuple.getT2();
        		
				access.fromTempRequestInfo(request.siteAccess, request.location, account.company, dataCenter.international != null ? dataCenter.international : false);
				
				String startTime = access.startDatetime + ":00Z";
				String endTime = access.endDatetime + ":00Z";
				
				OffsetDateTime endDateTime = OffsetDateTime.parse(endTime);
				OffsetDateTime startDateTime = OffsetDateTime.parse(startTime);
				
				long days = startDateTime.until(endDateTime, ChronoUnit.DAYS);
				
				if(days > 30)
				{
					throw localeService.createError("physical.access.invalid");
				}
				
				return physicalAccessRepository.save(access);
        	})
        	.flatMap(access -> {
				Map<String, Object> audit = new HashMap<String, Object>();
				
				audit.put("currentAccess", access.currentAccess);
				audit.put("currentAccessDateRange", access.currentAccessDateRange);
				audit.put("startDatetime", access.startDatetime);
				audit.put("endDatetime", access.endDatetime);
				audit.put("company", access.company);
				audit.put("dataCenter", access.dataCenter);
				audit.put("siteId", access.siteId);
				audit.put("firstName", access.firstName);
				audit.put("lastName", access.lastName);
				audit.put("email", access.email);
				audit.put("companyName", access.companyName);
				
				return auditService.audit(account.id, access.company, access.dataCenter, access.siteId, "ChangeUserSiteAccess", account.calculateRole(), audit)
						.map(ignore -> account);
			});
        })
        .then(Mono.just(Message.OK_MESSAGE));
	}
	
	private Mono<PhysicalAccess> createInternationTicket(PhysicalAccess access)
	{
		if(access.international)
		{
			ObjectNode node = JsonNodeFactory.instance.objectNode();
			
			node.put("token", serviceNowToken);
			
			return WebClient.create().get()
		    .uri(builder -> builder.scheme("http")
	                .host(serviceNowDomain).path("create-physical-access-incident/" + access.id)
	                .queryParam("token", serviceNowToken)
	                .build())
		    .retrieve().bodyToMono(String.class)
		    .map(ignore -> access);
		}
		
		return Mono.just(access);
	}
	
	@PostMapping("/set-access")
	@PreAuthorize("hasAuthority('ADMIN')")
	public Mono<Message> setPhysicalAccess(@RequestBody PhysicalAccessRequest request)
	{
		if(request.companyUserList == null)
		{
			request.companyUserList = new ArrayList<>();
		}
		
		return ReactiveSecurityContextHolder.getContext()
        .map(SecurityContext::getAuthentication)
        .map(Authentication::getPrincipal)
        .cast(Account.class)
        .flatMapMany(account -> {
        	return Flux.fromIterable(request.companyUserList)
        	.flatMap(userId -> 
        	{
        		return physicalAccessRepository.findFirstByUserAndDataCenter(userId, request.location.dataCenter)
        				.switchIfEmpty(Mono.defer(() -> {
        	        		PhysicalAccess access = new PhysicalAccess();
        	        		
        	        		return repositoryService.generateUniqueId(physicalAccessRepository)
        	        				.map(id -> {
        	        					access.id = id;
        	        					
        	        					return access;
        	        				});
        	        	}))
        				.flatMap(access -> setUserOnPhysicalAccess(access, userId))
        				.flatMap(access -> {
        					access.fromRequestInfo(request.siteAccess, request.location, userId, account.company);
        					
        					if(access.currentAccess.equals("Scheduled"))
        					{
        						String startTime = access.startDatetime + ":00Z";
        						String endTime = access.endDatetime + ":00Z";
        						
        						OffsetDateTime endDateTime = OffsetDateTime.parse(endTime);
        						OffsetDateTime startDateTime = OffsetDateTime.parse(startTime);
        						
        						long days = startDateTime.until(endDateTime, ChronoUnit.DAYS);
        						
        						if(days > 30)
        						{
        							throw localeService.createError("physical.access.invalid");
        						}
        					}
        					
        					return physicalAccessRepository.save(access);
        				})
        		        .flatMap(access -> {
        					Map<String, Object> audit = new HashMap<String, Object>();
        					
        					audit.put("currentAccess", access.currentAccess);
        					audit.put("currentAccessDateRange", access.currentAccessDateRange);
        					audit.put("startDatetime", access.startDatetime);
        					audit.put("endDatetime", access.endDatetime);
        					audit.put("user", access.user);
        					audit.put("company", access.company);
        					audit.put("dataCenter", access.dataCenter);
        					audit.put("siteId", access.siteId);
        					
        					return auditService.audit(account.id, access.company, access.dataCenter, access.siteId, "ChangeUserSiteAccess", account.calculateRole(), audit)
        							.map(ignore -> account);
        				});
        	});
        
        })
        .then(Mono.just(Message.OK_MESSAGE));
	}
	
	@PostMapping("/invite")
	@PreAuthorize("hasAuthority('SUPERADMIN') or hasAuthority('ADMIN') or hasAuthority('TECHNICALADMIN') or hasAuthority('BILLINGADMIN') or hasAuthority('CALLCENTERADMIN') or hasAuthority('GUARDADMIN')")
	public Mono<Message> inviteUser(ServerWebExchange exchange)
	{
		return ReactiveSecurityContextHolder.getContext()
        .map(SecurityContext::getAuthentication)
        .map(Authentication::getPrincipal)
        .cast(Account.class)
        .flatMap(account -> {
        	return exchange.getFormData()
        			.flatMap(formData -> {
        				String email = formData.getFirst("email");
        				String role = formData.getFirst("role");
        				
        				if(role == null || role.trim().equals(""))
        				{
        					throw localeService.createError("invite.role.invalid");
        				}
        				
        				Role actualRole = Role.valueOf(role);
        				
        				if(!canInviteRole(account, actualRole))
        				{
        					throw localeService.createError("access.denied");
        				}
        				
        				return accountRepository.findFirstByEmailIgnoreCase(email)
        						.switchIfEmpty(Mono.defer(() -> {
    								Account inviteAccount = new Account();
    								
    								inviteAccount.legacy = true;

    								return Mono.just(inviteAccount);
        						}))
        				.flatMap(inviteAccount -> {
        					if(!inviteAccount.legacy)
        					{
        						throw localeService.createError("register.email.exists");
        					}
        					
        					Invitation invite = new Invitation();
            				
            				if(companyRole(actualRole))
            				{
            					if(formData.containsKey("siteIds"))
            					{
            						invite.siteIds = formData.get("siteIds");
            					}
            					if(formData.containsKey("salesforceContactId"))
            					{
            						invite.salesforceId = formData.getFirst("salesforceContactId");
            					}
    	        				if(account.roles.contains(Role.SUPERADMIN))
    	        				{
    	        					if(formData.containsKey("company"))
    	        					{
    	        						invite.company = formData.getFirst("company");
    	        					}
    	        					else
    	        					{
    	        						throw localeService.createError("invite.company.invalid");
    	        					}
    	        				}
    	        				else
    	        				{
    	        					invite.company = account.company;
    	        				}
            				}
            				
            				if(dataCenterRole(actualRole))
            				{
    	        				if(account.roles.contains(Role.SUPERADMIN))
    	        				{
    	        					if(formData.containsKey("dataCenter"))
    	        					{
    	        						invite.dataCenter = formData.getFirst("dataCenter");
    	        					}
    	        					else
    	        					{
    	        						throw localeService.createError("invite.datacenter.invalid");
    	        					}
    	        				}
    	        				else
    	        				{
    	        					invite.dataCenter = account.dataCenter;
    	        				}
            				}
            				
            				invite.email = email;
            				invite.invitedBy = account.email;
            				invite.roles.add(actualRole);
            				
            				Map<String, Object> audit = new HashMap<String, Object>();
            				
            				audit.put("salesforceContactId", invite.salesforceId);
            				audit.put("company", invite.company);
            				audit.put("dataCenter", invite.dataCenter);
            				audit.put("email", email);
            				audit.put("role", role);
            				
            				String token = invitationTokenService.encode(invite);
            				Map<String, Object> variables = new HashMap<String, Object>();
        					
            				try
            				{
            					variables.put("url", "http://" + serverDomain + "/#/auth/register?token=" + URLEncoder.encode(token, "UTF-8"));
            				}
            				catch(Exception e)
            				{
            				}

        					return emailService.sendEmail(email, "Invitation to Evoque Portal", "invite", variables)
        							.flatMap(ignore -> {
        								if(inviteAccount.id != null && !inviteAccount.id.trim().equals(""))
        								{
        									inviteAccount.portalStatus = "Invited";
        									
        									return accountRepository.save(inviteAccount);
        								}
        								
        								return Mono.just(inviteAccount);
        							})
        							.flatMap(ignore -> changeSalesforceProfileStatus(invite.salesforceId, "Invited"))
        							.flatMap(ignore -> auditService.audit(account.id, account.company, account.dataCenter, null, "UserInvited", account.calculateRole(), audit));
        					
        				});
        				
        				
        			});
        })
        .map(ignore -> Message.OK_MESSAGE);
	}
	
	private boolean companyRole(Account account)
	{
		if(account.roles.contains(Role.ADMIN) || account.roles.contains(Role.TECHNICALADMIN) || account.roles.contains(Role.TECHNICALUSER)
				 || account.roles.contains(Role.BILLINGADMIN) || account.roles.contains(Role.BILLINGUSER))
		{
			return true;
		}
		
		return false;
	}
	
	private boolean companyRole(Role role)
	{
		if(role == Role.ADMIN || role == Role.TECHNICALADMIN || role == Role.TECHNICALUSER
				 || role == Role.BILLINGADMIN || role == Role.BILLINGUSER)
		{
			return true;
		}
		
		return false;
	}
	
	private boolean dataCenterRole(Role role)
	{
		if(role == Role.GUARDADMIN || role == Role.GUARDUSER)
		{
			return true;
		}
		
		return false;
	}
	
	private boolean canInviteRole(Account account, Role role)
	{
		if(account.roles.contains(Role.SUPERADMIN))
		{
			return true;
		}
		
		if(account.roles.contains(Role.ADMIN) && (role == Role.ADMIN || role == Role.TECHNICALADMIN || role == Role.TECHNICALUSER || role == Role.BILLINGADMIN || role == Role.BILLINGUSER))
		{
			return true;
		}
		
		if(account.roles.contains(Role.TECHNICALADMIN) && (role == Role.TECHNICALADMIN || role == Role.TECHNICALUSER))
		{
			return true;
		}
		
		if(account.roles.contains(Role.BILLINGADMIN) && (role == Role.BILLINGADMIN || role == Role.BILLINGUSER))
		{
			return true;
		}
		
		if(account.roles.contains(Role.GUARDADMIN) && (role == Role.GUARDADMIN || role == Role.GUARDUSER))
		{
			return true;
		}
		
		if(account.roles.contains(Role.CALLCENTERADMIN) && (role == Role.CALLCENTERADMIN || role == Role.CALLCENTERUSER))
		{
			return true;
		}
		
		return false;
	}
	
	@PostMapping("/update-user/{userId}")
	@PreAuthorize("hasAuthority('ADMIN') or hasAuthority('SUPERADMIN')")
	public Mono<Message> updateUser(ServerWebExchange exchange, @PathVariable("userId") String userId)
	{
		return ReactiveSecurityContextHolder.getContext()
		        .map(SecurityContext::getAuthentication)
		        .map(Authentication::getPrincipal)
		        .cast(Account.class)
		        .zipWith(accountRepository.findById(userId))
		        .flatMap(tuple -> {
		        	Account account = tuple.getT1();
		        	Account user = tuple.getT2();
		        	
		        	if(account.calculateRole() == Role.ADMIN && (account.company == null || account.company.trim().equals("") 
		        			|| user.company == null || !account.company.equals(user.company)))
		        	{
		        		throw localeService.createError("access.denied");
		        	}
		        	
		        	return exchange.getFormData()
		        			.flatMap(formData -> {
		        				String role = formData.getFirst("role");
		        				
		        				if(role == null || role.trim().equals(""))
		        				{
		        					throw localeService.createError("invite.role.invalid");
		        				}
		        				
		        				if(role != null && !role.trim().equals(""))
		        				{
			        				Role actualRole = Role.valueOf(role);
			        				
			        				if(!canInviteRole(account, actualRole))
			        				{
			        					throw localeService.createError("access.denied");
			        				}
			        				
			        				user.roles.clear();
			        				user.roles.add(actualRole);
		        				}
		        				
		        				user.siteIds = formData.get("siteIds");
		        				
		        				if(account.calculateRole() == Role.SUPERADMIN)
		        				{
		        					String dataCenter = formData.getFirst("dataCenter");
		        						
		        					if(dataCenter != null && !dataCenter.trim().equals(""))
		        					{
		        						user.dataCenter = dataCenter;
		        					}
		        				}
		        				
		        				return accountRepository.save(user);
		        			});
		        })
		        .then(Mono.just(Message.OK_MESSAGE));
	}
	
	@PostMapping("/register")
	public Mono<Message> registerEmail(ServerWebExchange exchange)
	{
		return exchange.getFormData()
			.flatMap(formData -> {
				String email = formData.getFirst("email");
				String firstName = formData.getFirst("firstname");
				String lastName = formData.getFirst("lastname");
				String phoneNumber = formData.getFirst("phonenumber");
				String password = formData.getFirst("password");
				String invite = formData.getFirst("invite");
				Invitation inviteToken = null;
				
				if(email == null || email.trim().equals("") || !validationService.isValidEmail(email))
				{
					throw localeService.createError("register.email.invalid");
				}
				
				if(password == null || password.trim().equals(""))
				{
					throw localeService.createError("register.password.invalid");
				}
				
				if(invite == null || invite.trim().equals(""))
				{
					throw localeService.createError("register.invitation.invalid");
				}
				else
				{
					try
					{
						inviteToken = invitationTokenService.decode(invite);
					}
					catch(Exception e)
					{
						throw localeService.createError("register.invitation.invalid");
					}
					
					if(inviteToken == null)
					{
						throw localeService.createError("register.invitation.invalid");
					}
					
					if(!inviteToken.email.equalsIgnoreCase(email))
					{
						throw localeService.createError("register.email.invalid");
					}
				}
				
				final Invitation invitation = inviteToken;
				
				return accountRepository.findFirstByEmailIgnoreCase(email)
						.switchIfEmpty(Mono.defer(() -> {
							return repositoryService.generateUniqueId(accountRepository).map(id ->{
								Account account = new Account();
								
								account.id = id;
								account.legacy = true;

								return account;
							});
						}))
						.map(existingAccount -> {
							if(!existingAccount.enabled)
							{
								throw localeService.createError("access.denied");
							}
							
							if(!existingAccount.legacy)
							{
								throw localeService.createError("register.email.exists");
							}
							
							existingAccount.email = email;
							existingAccount.password = passwordEncoder.encode(password);
							existingAccount.firstName = firstName;
							existingAccount.lastName = lastName;
							existingAccount.phoneNumber = phoneNumber;
							existingAccount.company = invitation.company;
							existingAccount.dataCenter = invitation.dataCenter;
							existingAccount.roles = invitation.roles;
							
							if(invitation.siteIds != null && !invitation.siteIds.isEmpty())
							{
								existingAccount.siteIds = invitation.siteIds;
							}
							
							existingAccount.enabled = true;
							existingAccount.legacy = false;
							existingAccount.portalStatus = "Registered";
							
							return existingAccount;
						})
						.flatMap(account -> {
								return changeSalesforceProfileStatus(invitation.salesforceId, "Registered")
										.switchIfEmpty(Mono.just("OK"))
										.map(ignore -> {
											return account;
										}).doOnError(ex -> ex.printStackTrace());
						});
			})
			.flatMap(account -> {
				return accountRepository.save(account);
			})
			.flatMap(account -> {
				Map<String, Object> audit = new HashMap<String, Object>();
				
				audit.put("email", account.email);
				audit.put("firstName", account.firstName);
				audit.put("lastName", account.lastName);
				
				return auditService.audit(account.id, account.company, account.dataCenter, null, "UserRegistered", account.calculateRole(), audit)
						.map(ignore -> account);
			})
			.flatMap(account -> {
				if(companyRole(account) && (account.serviceNowContact == null || account.serviceNowContact.trim().equals("")))
				{
					ObjectNode node = JsonNodeFactory.instance.objectNode();
					
					node.put("token", serviceNowToken);
					node.put("account", account.id);
					
					return WebClient.create().post()
				    .uri(builder -> builder.scheme("http")
			                .host(serviceNowDomain).path("create-contact")
			                .build())
				    .contentType(MediaType.APPLICATION_JSON)
				    .syncBody(node)
				    .retrieve().bodyToMono(String.class);
				}
				
				return Mono.just("OK");
			})
			.switchIfEmpty(Mono.just("OK"))
			.map(ignore -> {
				return Message.OK_MESSAGE;
			});
	}
	
//	private Flux<SalesforceContact> retrieveSalesforceContacts(String company)
//	{
//		return WebClient.create().get()
//				.uri(builder -> builder.scheme("http")
//                .host(salesforceDomain).path("contacts/" + company)
//                .queryParam("token", salesforceToken)
//                .build())
//	    .retrieve()
//	    .bodyToFlux(SalesforceContact.class);
//	}
	
//	private Mono<SalesforceContactAccount> retrieveSalesforceContactAccount(String domain)
//	{
//		return WebClient.create().get()
//				.uri(builder -> builder.scheme("http")
//                .host(salesforceDomain).path("search-domain")
//                .queryParam("token", salesforceToken)
//                .queryParam("domain", domain)
//                .build())
//	    .retrieve()
//	    .bodyToMono(SalesforceContactAccount.class);
//	}
//	
//	private Mono<LabelValuePair> retrieveSalesforceDataCenter(String siteId)
//	{
//		return WebClient.create().get()
//				.uri(builder -> builder.scheme("http")
//                .host(salesforceDomain).path("datacenter-siteid/" + siteId)
//                .queryParam("token", salesforceToken)
//                .build())
//	    .retrieve()
//	    .bodyToMono(LabelValuePair.class);
//	}
	
	private Mono<Boolean> changeSalesforceProfileStatus(Account account, String status)
	{
		if(account == null || account.salesforceContacts == null || account.salesforceContacts.isEmpty())
		{
			return Mono.just(false);
		}
		
		return Flux.fromIterable(account.salesforceContacts)
		.flatMap(salesforceId -> changeSalesforceProfileStatus(salesforceId, status))
		.then(Mono.just(true));
		
	}
	
	private Mono<String> changeSalesforceProfileStatus(String salesforceId, String status)
	{
		if(salesforceId == null || salesforceId.trim().equals(""))
		{
			return Mono.just("-");
		}
		
		ObjectNode node = JsonNodeFactory.instance.objectNode();
		
		node.put("token", salesforceToken);
		node.put("salesforceId", salesforceId);
		node.put("status", status);
		
		return WebClient.create().post()
	    .uri(builder -> builder.scheme("http")
                .host(salesforceDomain).path("update-contact-status")
                .build())
	    .contentType(MediaType.APPLICATION_JSON)
	    .syncBody(node)
	    .retrieve().bodyToMono(String.class)
	    .defaultIfEmpty("-");
	}
}
