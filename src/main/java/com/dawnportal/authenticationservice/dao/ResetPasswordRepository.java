package com.dawnportal.authenticationservice.dao;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import com.dawnportal.authenticationservice.model.ResetPassword;

@Repository
public interface ResetPasswordRepository extends ReactiveMongoRepository<ResetPassword, String> {
}
